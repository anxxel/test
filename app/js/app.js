'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngRoute',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers',
  'ngAnimate',
  'ngSanitize',
  'mgcrea.ngStrap'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/movies', {templateUrl: 'partials/partial1.html', controller: 'TypeaheadDemoCtrl'});
  $routeProvider.when('/movies/:movieId', {templateUrl: 'partials/partial2.html', controller: 'MovieDetailCtrl'});
  $routeProvider.otherwise({redirectTo: '/movies'});
}]).
config(function($typeaheadProvider) {
  angular.extend($typeaheadProvider.defaults, {
    animation: 'am-flip-x',
    minLength: 3,
    limit: 5
  });
});
