'use strict';

/* Controllers */

angular.module('myApp.controllers', []).
  controller('MainCtrl', function($scope) {
  })
  .controller('TypeaheadDemoCtrl', function($scope, $templateCache, $http, $location, Movies) { 
    $scope.movies = Movies.query();
    $scope.selectedMovie = '';
    $scope.predicate = 'slug';

    $scope.goTo = function (path) {
      console.log(path);
      $location.path(path);
    };

}).controller('MovieDetailCtrl', ['$scope', '$routeParams', 'Movies',
  function($scope, $routeParams, Movies) {
    $scope.movie = Movies.query({movieId: $routeParams.movieId}, function(movie) {
      
    });
}]);