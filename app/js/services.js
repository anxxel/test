'use strict';

/* Services */

angular.module('myApp.services', ['ngResource'])
.factory('Movies', ['$resource',
	function($resource){
	return $resource('data/results.json', {}, {
	  query: {method:'GET', isArray:true}
	});
}]);